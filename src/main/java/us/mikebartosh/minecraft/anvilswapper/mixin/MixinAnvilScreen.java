package us.mikebartosh.minecraft.anvilswapper.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.DrawContext;
import net.minecraft.client.gui.screen.ingame.AnvilScreen;
import net.minecraft.client.gui.screen.ingame.ForgingScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AnvilScreenHandler;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.At;
import us.mikebartosh.minecraft.anvilswapper.AnvilSwapper;

@Mixin(AnvilScreen.class)
public abstract class MixinAnvilScreen
        extends ForgingScreen<AnvilScreenHandler> {

    @Shadow
    @Final
    private PlayerEntity player;

    @Shadow
    public abstract void onSlotUpdate(ScreenHandler handler, int slotId, ItemStack stack);

    @Unique
    private int current_position_exp_cost = 1;

    public MixinAnvilScreen(AnvilScreenHandler handler, PlayerInventory playerInventory, Text title, Identifier texture) {
        super(handler, playerInventory, title, texture);
    }

    @Inject(method = "drawBackground", at = @At("TAIL"))
    protected void $anvil_swapper_OnDrawBackground(DrawContext context, float delta, int mouseX, int mouseY, CallbackInfo ci) {
        String button_path;
        final boolean cursor_hovered = (mouseX >= this.x + 5 && mouseX < this.x + 5 + 18 && mouseY >= this.y + 46 && mouseY < this.y + 46 + 18);

        // If the player clicked on the button we need to show that the button is being pressed
        // Only if we are above it, no other action can happen so this is the final button state
        if (AnvilSwapper.left_mouse_clicked && cursor_hovered) {
            button_path = "pressed";

            // We simulate clicks on each item, so we can swap them
            assert MinecraftClient.getInstance().interactionManager != null;
            MinecraftClient.getInstance().interactionManager.clickSlot(this.handler.syncId, 0, 0, SlotActionType.PICKUP, player);
            MinecraftClient.getInstance().interactionManager.clickSlot(this.handler.syncId, 1, 0, SlotActionType.PICKUP, player);
            MinecraftClient.getInstance().interactionManager.clickSlot(this.handler.syncId, 0, 0, SlotActionType.PICKUP, player);
        } else {
            // If the mouse is over the button we need to show that the button is hovered, this is the final state then
            // Otherwise the button is unpressed
            if (cursor_hovered) {
                button_path = "hovered";
            } else {
                button_path = "unpressed";
            }
        }

        // We need to check if one of the slots are empty, if so we don't care about showing it as swap recommended
        final boolean is_slot_empty = this.handler.getSlot(0).getStack().isEmpty() || this.handler.getSlot(1).getStack().isEmpty();

        // If the current cost is more than the swap, we need to show the button as swap recommended, unless a slot is empty
        if ((current_position_exp_cost > AnvilSwapper.swapped_position_exp_cost) && !is_slot_empty) {
            button_path = "textures/gui/anvil-swapper-buttons/anvil_button_" + button_path + "_swap.png";
        } else {
            // Otherwise we need to show the button as swap not recommended
            button_path = "textures/gui/anvil-swapper-buttons/anvil_button_" + button_path + ".png";
        }

        // Reset the left mouse click, so we don't keep clicking the button
        AnvilSwapper.left_mouse_clicked = false;

        // Finally draw the button
        context.drawTexture(Identifier.of("anvil-swapper", button_path), this.x + 5, this.y + 46, 0, 0, 18, 18, 18, 18);
    }

    @Inject(method = "drawForeground", at = @At("HEAD"))
    protected void $anvilsavings_OnAnvilScreen(DrawContext context, int mouseX, int mouseY, CallbackInfo ci) {
        // This is a quick and dirty way to get the first level cost, we need to store it, so we can compare it later
        // Also saves on recalculating it
        current_position_exp_cost = ((AnvilScreenHandler) this.handler).getLevelCost();
    }
}
