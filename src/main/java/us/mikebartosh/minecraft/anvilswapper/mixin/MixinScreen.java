package us.mikebartosh.minecraft.anvilswapper.mixin;

import net.minecraft.client.gui.AbstractParentElement;
import net.minecraft.client.gui.screen.Screen;
import org.spongepowered.asm.mixin.Mixin;
import us.mikebartosh.minecraft.anvilswapper.AnvilSwapper;

@Mixin(Screen.class)
public abstract class MixinScreen extends AbstractParentElement {

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int button) {
        // If the left mouse button is clicked, set the left_mouse_clicked variable to true
        if (button == 0) {
            AnvilSwapper.left_mouse_clicked = true;
        }
        return super.mouseClicked(mouseX, mouseY, button);
    }

    @Override
    public boolean mouseReleased(double mouseX, double mouseY, int button) {
//        if (button == 0) {
//            AnvilSwapper.hovered = false;
//        }
//
//        System.out.println("Mouse released");
//
//        this.setDragging(false);
//        return this.hoveredElement(mouseX, mouseY).filter(element -> element.mouseReleased(mouseX, mouseY, button)).isPresent();
        return super.mouseReleased(mouseX, mouseY, button);
    }
}
